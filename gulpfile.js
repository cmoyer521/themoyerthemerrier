var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var cleancss = require('gulp-clean-css');
var rename = require('gulp-rename');
var gzip = require('gulp-gzip');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var gzip_options = {
    threshold: '1kb',
    gzipOptions: {
        level: 9
    }
};

// styles
gulp.task('scss', function() {
    return gulp.src([
        'public/Static/scss/styles.scss'
    ])
    .pipe(sass())
    .pipe(gulp.dest('public/Static/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleancss())
    .pipe(gulp.dest('public/Static/css'))
});


gulp.task('compress-styles', function() {
    return gulp.src([
        'public/Static/css/styles.css'
    ])
    .pipe(gzip(gzip_options))
    .pipe(gulp.dest('public/Static/gz'))
    .pipe(notify({
        message: 'Styles task complete',
        onLast: 'True'
    }))
});

gulp.task('styles', gulp.series('scss', 'compress-styles'))

// Scripts


gulp.task('watch', function() {
    gulp.watch('public/Static/scss/*.scss', gulp.series('styles'));
});

gulp.task('default', gulp.parallel('styles', 'watch'));