$(document).ready(function() {
    var weddingDate = new Date("Sept 26, 2020 16:30:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {
        // Get today's date and time
        var now = new Date().getTime();
        // console.log(now);

        // Find the difference and split into days, hours, minutes, seconds
        var difference = weddingDate - now;
        var days = Math.floor(difference / (1000 * 60 * 60 *24));
        var hours = Math.floor((difference % (1000 * 60 * 60 *24)) / (1000 * 60 * 60));
        var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((difference % (1000 * 60)) / 1000);

        // Update our boxes;
        $(".days-box .box-value").text(days);
        $(".hours-box .box-value").text(hours);
        $(".minutes-box .box-value").text(minutes);
        $(".seconds-box .box-value").text(seconds);

        if (difference < 0) {
            clearInterval(x);
            $(".countdown-boxes-row").html("WE'RE MARRIED");
        }
    }, 1000)
})